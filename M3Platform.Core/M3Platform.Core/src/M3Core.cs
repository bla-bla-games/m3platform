﻿﻿using System.Collections.Generic;
using M3Platform.Core.Board;
using M3Platform.Core.Board.Obstacle;
 using M3Platform.Core.Board.State;
 using M3Platform.Core.Board.Token;
 using M3Platform.Core.Bonus;
 using M3Platform.Core.Combination;
using M3Platform.Core.Combination.Finder;
using M3Platform.Core.Combination.Processor;
using M3Platform.Core.Filler;
using M3Platform.Core.Generator;
using UnityEngine;

namespace M3Platform.Core
{
    public class M3Core
    {
        private M3Board _board;
        private M3ItemTypesStorage _typesStorage;
        private IM3CombinationsFinder _actualCombinationsFinder;
        private IM3PossibleCombinationsFinder _posibleCombinationsFinder;
        private IM3CombinationsProcessor _combinationsProcessor;
        private IM3EmptySlotsFiller _emptySlotsFiller;
        private IM3TokensGenerator _tokensGenerator;
        private IM3BonusFinder _bonusFinder;

        private bool _isBoardInvalidated;
        private List<M3Combination> _cachedCombinations;

        public M3Core()
        {
            _typesStorage = new M3ItemTypesStorage();

            _board = new M3Board();
            _board.OnChanged += OnBoardChanged;
            
            _actualCombinationsFinder = CreateActualCombinationsFinder();
            _posibleCombinationsFinder = CreatePosibleCombinationsFinder();
            _combinationsProcessor = CreateCombinationsProcessor();
            _emptySlotsFiller = CreateEmptySlotsFiller();
            _tokensGenerator = CreateTokensGenerator();
            _bonusFinder = CreateBonusFinder();
        }

        protected virtual IM3CombinationsFinder CreateActualCombinationsFinder()
        {
            return new M3DefaultCombinationsFinder(_board, _typesStorage);
        }
        
        protected virtual IM3PossibleCombinationsFinder CreatePosibleCombinationsFinder()
        {
            return new M3DefaultPossibleCombinationsFinder(_board, _actualCombinationsFinder);
        }

        protected virtual IM3CombinationsProcessor CreateCombinationsProcessor()
        {
            return new M3DefaultCombinationsProcessor();
        }

        protected virtual IM3EmptySlotsFiller CreateEmptySlotsFiller()
        {
            return new M3DefaultEmptySlotsFiller();
        }

        protected virtual IM3TokensGenerator CreateTokensGenerator()
        {
            return new M3DefaultTokensGenerator(_board, _typesStorage);
        }
        
        protected virtual IM3BonusFinder CreateBonusFinder()
        {
            return new M3DefaultBonusFinder();
        }

        private void OnBoardChanged()
        {
            _isBoardInvalidated = true;
        }

        /// <summary>
        /// Configures the field and creates slots.
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        public void Configure(int rows, int columns)
        {
            _board.Configure(rows, columns);
        }

        /// <summary>
        /// Registers token type.
        /// </summary>
        /// <param name="typeData"></param>
        public void RegisterTokenType(M3TokenTypeData typeData)
        {
            _typesStorage.RegisterTokenType(typeData);
        }

        public M3TokenTypeData UnregisterTokenType(string id)
        {
            return _typesStorage.UnregisterTokenType(id);
        }
        
        /// <summary>
        /// Registers obstacle type.
        /// </summary>
        /// <param name="typeData"></param>
        public M3ObstacleTypeData RegisterObstacleType(M3ObstacleTypeData typeData)
        {
            _typesStorage.RegisterObstacleType(typeData);
            return typeData;
        }

        public M3ObstacleTypeData UnregisterObstacleType(string id)
        {
            return _typesStorage.UnregisterObstacleType(id);
        }

        /// <summary>
        /// Initialize game board.
        /// </summary>
        public void Initialize()
        {
            _isBoardInvalidated = true;
            _cachedCombinations = null;
            
            if (_typesStorage.TokenTypesCount <= 0)
            {
                Debug.LogWarning("You sould to register at least on item type!");
                return;
            }

            // Fill Field by using registered types

            for (int row = 0; row < _board.Size.Rows; row++)
            {
                for (int column = 0; column < _board.Size.Columns; column++)
                {
                    if (!_board.HasSlotAt(row, column))
                    {
                        continue;
                    }

                    M3TokenData tokenData = _tokensGenerator.GenerateTokenFor(row, column);
                    _board.InsertTokenAt(row, column, tokenData);
                }
            }
        }

        // temp method to fill board with template
        public void FillWithTemplate(string[,] template)
        {
            if (_typesStorage.TokenTypesCount <= 0)
            {
                Debug.LogWarning("You sould to register at least on item type!");
                return;
            }

            // Fill Field by using registered types

            for (int row = 0; row < _board.Size.Rows; row++)
            {
                for (int column = 0; column < _board.Size.Columns; column++)
                {
                    if (!_board.HasSlotAt(row, column))
                    {
                        continue;
                    }

                    if (template[row, column] == null)
                    {
                        continue;
                    }

                    M3TokenTypeData tokenTypeData = _typesStorage.GetTokenType(template[row, column]);
                    _board.InsertTokenAt(row, column, new M3TokenData(tokenTypeData.Id));
                }
            }
        }

        /// <summary>
        /// Returns actual combinations on the board. 
        /// If the board was not changed from previous call, the result will be gotten from cache.
        /// </summary>
        /// <returns></returns>
        public List<M3Combination> GetActualCombinations()
        {
            if (_isBoardInvalidated || _cachedCombinations == null)
            {
                _cachedCombinations = _actualCombinationsFinder.GetActualCombinations();
                _isBoardInvalidated = false;
            }

            return _cachedCombinations;
        }

        public List<M3Combination> GetPossibleCombinations()
        {
            return _posibleCombinationsFinder.GetPosibleCombinations();
        }
 
        public void ProcessCombinations()
        {
            _combinationsProcessor.Process(this, GetActualCombinations());
        }

        public List<M3TokenMovementData> CollapseFloatingTokens()
        {
            return _emptySlotsFiller.CollapseFloatingTokens(_board);
        }

        public List<M3TokenMovementData> FillRemainingEmptySlots()
        {
            return _emptySlotsFiller.DropNewTokens(_board, _tokensGenerator);
        }

        public void PrintBoardSnapshot()
        {
            _board.Snapshot();
        }

        public T GetTokenType<T>(string typeId) where T : M3TokenTypeData
        {
            return _typesStorage.GetTokenType(typeId) as T;
        }
        
        public T GetObstacleType<T>(string typeId) where T : M3ObstacleTypeData
        {
            return _typesStorage.GetObstacleType(typeId) as T;
        }

        public M3Board Board
        {
            get { return _board; }
        }

        public IM3BonusFinder BonusFinder
        {
            get { return _bonusFinder; }
        }

        public M3ItemTypesStorage TypesStorage
        {
            get { return _typesStorage; }
        }

        public IM3TokensGenerator TokensGenerator
        {
            get { return _tokensGenerator; }
        }
    }
}