﻿using System.Collections.Generic;
using System.Linq;
using M3Platform.Core.Board;
using M3Platform.Core.Board.Obstacle;
using M3Platform.Core.Board.State;
using M3Platform.Core.Board.Token;

namespace M3Platform.Core.Combination.Processor
{
    public class M3DefaultCombinationsProcessor : IM3CombinationsProcessor
    {
        public void Process(M3Core core, List<M3Combination> combinations)
        {
            M3BoardChangesDescription description = new M3BoardChangesDescription();
            
            foreach (M3Combination combination in combinations)
            {
                ProcessSingleCombination(core, combination, description);
            }
        }

        protected virtual void ProcessSingleCombination(M3Core core, M3Combination combination, M3BoardChangesDescription description)
        {
            List<M3SlotData> slotsAroundCombination = core.Board.GetSlotsAroundCombination(combination);
            
            foreach (M3SlotData slotData in slotsAroundCombination)
            {
                if (!slotData.HasObstacle)
                {
                    continue;
                }
                
                M3ObstacleData obstacleData = slotData.GetObstacle();
                M3ObstacleTypeData type = core.TypesStorage.GetObstacleType(obstacleData.Type);

                if (!type.IsDestroyable)
                {
                    continue;
                }

                int previousHealth = obstacleData.Health;
                obstacleData.DecreaseHealth();
                    
                if (obstacleData.Health <= 0)
                {
                    slotData.RemoveObstacle();
                }
                    
                M3TriggerSlotObstacleDescription slotObstacleDescription = new M3TriggerSlotObstacleDescription(slotData.Id, obstacleData, previousHealth);
                description.TriggeredSlotObstacles.Add(slotObstacleDescription);
            }
            
            foreach (M3SlotData slotData in combination)
            {
                if (slotData.TokenData.HasObstacle)
                {
                    M3ObstacleData obstacleData = slotData.TokenData.GetObstacle();
                    M3ObstacleTypeData type = core.TypesStorage.GetObstacleType(obstacleData.Type);
                    
                    if (!type.IsDestroyable)
                    {
                        continue;
                    }
                    
                    int previousHealth = obstacleData.Health;
                    obstacleData.DecreaseHealth();

                    if (obstacleData.Health <= 0)
                    {
                        slotData.TokenData.RemoveObstacle();
                    }
                    
                    M3TriggerTokenObstacleDescription tokenObstacleDescription = new M3TriggerTokenObstacleDescription(slotData.Id, slotData.TokenData, obstacleData, previousHealth);
                    description.TriggeredTokenObstacles.Add(tokenObstacleDescription);
                    
                    continue;
                }
                
                M3GatherTokenDescription gatherTokenDescription = new M3GatherTokenDescription(slotData.Id, slotData.TokenData);
                core.Board.ClearSlotAt(slotData.Position.Row, slotData.Position.Column);
                description.GatherTokensFromSlots.Add(gatherTokenDescription);
            }
            
            TryCraftBonusFrom(core, combination, description);
        }

        private void TryCraftBonusFrom(M3Core core, M3Combination combination, M3BoardChangesDescription description)
        {
            string bonusId = core.BonusFinder.FindBonusIn(combination);

            if (string.IsNullOrEmpty(bonusId))
            {
                return;
            }

            string bonusTokenTypeId = core.TypesStorage.GetBonusTokenTypeId(combination.TokenType, bonusId);
            
            M3CraftBonusDescription bonusDescription = new M3CraftBonusDescription(bonusTokenTypeId, combination);
            description.CraftedBonuses.Add(bonusDescription);

            foreach (M3SlotData slotData in combination)
            {
                core.Board.ClearSlotAt(slotData.Position);
            }

            M3TokenData bonusTokenData = core.TokensGenerator.CreateToken(bonusTokenTypeId);
            core.Board.InsertTokenAt(combination.LeadSlot.Position, bonusTokenData);
        }
    }
}