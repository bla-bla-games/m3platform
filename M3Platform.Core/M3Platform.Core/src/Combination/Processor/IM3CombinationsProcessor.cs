﻿using System.Collections.Generic;
using M3Platform.Core.Board;
using M3Platform.Core.Board.State;

namespace M3Platform.Core.Combination.Processor
{
    public interface IM3CombinationsProcessor
    {
        void Process(M3Core core, List<M3Combination> combinations);
    }
}