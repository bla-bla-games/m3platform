﻿namespace M3Platform.Core.Combination
{
    public enum M3CombinationOrientation
    {
        Horizontal = 0,
        Vertical,
        Mixed,
        Unknown
    }
}