﻿using M3Platform.Core.Combination;

namespace M3Platform.Core.M3Platform.Core.src.Combination.Contract
{
    public class M3CombinationContract
    {
        public string TokenTypeId { get; private set; }
        public int CombinationLength { get; private set; }
        public M3CombinationOrientation Orientation { get; private set; }
        
        public M3CombinationContract(string tokenTypeId, int combinationLength, M3CombinationOrientation orientation)
        {
            TokenTypeId = tokenTypeId;
            CombinationLength = combinationLength;
            Orientation = orientation;
        }

        public bool IsCombinationStartsContract(M3Combination combination)
        {
            if (combination.Orientation != Orientation)
            {
                return false;
            }
            
            if (combination.Count != CombinationLength)
            {
                return false;
            }

            return true;
        }
    }
}