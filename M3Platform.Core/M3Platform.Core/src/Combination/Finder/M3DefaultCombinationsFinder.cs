﻿﻿using System.Collections.Generic;
using System.Linq;
using M3Platform.Core.Board;
using M3Platform.Core.Board.Token;
 using UnityEngine;

namespace M3Platform.Core.Combination.Finder
{
    public class M3DefaultCombinationsFinder : IM3CombinationsFinder
    {
        private M3Board _board;
        private IM3ItemTypesDataProvider _typesDataProvider;
        private HashSet<M3SlotData> _usedSlots;

        public M3DefaultCombinationsFinder(M3Board board, IM3ItemTypesDataProvider typesDataProvider)
        {
            _board = board;
            _typesDataProvider = typesDataProvider;
            _usedSlots = new HashSet<M3SlotData>();
        }
        
        /// <summary>
        /// Seeking for all actual combinations on the board.
        /// </summary>
        /// <param name="board"></param>
        /// <param name="typesDataProvider"></param>
        /// <returns></returns>
        public List<M3Combination> GetActualCombinations()
        {
            List<M3Combination> horizontalCombinations = GetHorizontalCombinations();
            _usedSlots.Clear();
            
            List<M3Combination> verticalCombinations = GetVerticalCombinations();
            _usedSlots.Clear();
            
            List<M3Combination> result = horizontalCombinations.Concat(verticalCombinations).ToList();

            // external loop should have more items
            List<M3Combination> firstList = horizontalCombinations;
            List<M3Combination> secondList = verticalCombinations;

            if (secondList.Count > firstList.Count)
            {
                firstList = verticalCombinations;
                secondList = horizontalCombinations;
            }
            //

            // combine adjacent combinations
            foreach (M3Combination combination1 in firstList)
            {
                foreach (M3Combination combination2 in secondList)
                {
                    if (combination1.IsAdjacentWith(combination2))
                    {
                        combination1.Append(combination2);
                        result.Remove(combination2);
                        break;
                    }
                }
            }
            
            foreach (M3Combination combination in result)
            {
                if (_board.LastSwapInfo.Slot1 != null)
                {
                    if (combination.Contains(_board.LastSwapInfo.Slot1))
                    {
                        combination.LeadSlot = _board.LastSwapInfo.Slot1;
                    }
                }
                
                if (_board.LastSwapInfo.Slot2 != null)
                {
                    if (combination.Contains(_board.LastSwapInfo.Slot2))
                    {
                        combination.LeadSlot = _board.LastSwapInfo.Slot2;
                    }
                }

                if (combination.LeadSlot == null)
                {
                    combination.LeadSlot = combination.FirstOrDefault();
                }
            }
            
            return result;
        }

        public M3Combination GetActualCombinationAtPosition(int row, int column)
        {
            M3Combination result;
            
            M3Combination horizontalCombination = GetHorizontalCombinationForSlotAt(row, column);
            _usedSlots.Clear();
            
            M3Combination verticalCombination = GetVerticalCombinationForSlotAt(row, column);
            _usedSlots.Clear();

            if (horizontalCombination == null && verticalCombination == null)
            {
                return null;
            }
            
            result = new M3Combination();

            if (horizontalCombination != null)
            {
                result.Append(horizontalCombination);
            }
            
            if (verticalCombination != null)
            {
                result.Append(verticalCombination);
            }

            return result;
        }

        public M3Combination GetActualCombinationAtPosition(M3Position position)
        {
            return GetActualCombinationAtPosition(position.Row, position.Column);
        }

        /// <summary>
        /// Checks horizontal combinations
        /// </summary>
        /// <param name="board"></param>
        /// <param name="typesDataProvider"></param>
        /// <returns></returns>
        private List<M3Combination> GetHorizontalCombinations()
        {
            List<M3Combination> result = new List<M3Combination>();

            int margin = 2;
            if (_board.Size.Columns / 2 <= margin) margin = 0;

            for (int row = 0; row < _board.Size.Rows; row++)
            {
                for (int column = margin; column < _board.Size.Columns - margin; column++)
                {
                    M3Combination combination = GetHorizontalCombinationForSlotAt(row, column);
                    if (combination != null) result.Add(combination);
                }
            }

            return result;
        }
        
        /// <summary>
        /// Checks vertical combinations
        /// </summary>
        /// <param name="board"></param>
        /// <param name="typesDataProvider"></param>
        /// <returns></returns>
        private List<M3Combination> GetVerticalCombinations()
        {
            List<M3Combination> result = new List<M3Combination>();
            
            int margin = 2;
            if (_board.Size.Rows / 2 <= margin) margin = 0;

            for (int column = 0; column < _board.Size.Columns; column++)
            {
                for (int row = margin; row < _board.Size.Rows - margin; row++)
                {
                    M3Combination combination = GetVerticalCombinationForSlotAt(row, column);
                    if (combination != null) result.Add(combination);
                }
            }

            return result;
        }

        private M3Combination GetHorizontalCombinationForSlotAt(int row, int column)
        {
            if (!_board.HasSlotWithTokenAt(row, column))
            {
                return null;
            }

            if (_board.HasSlotObstacleAt(row, column))
            {
                return null;
            }

            M3SlotData sourceSlotData = _board.GetSlotAt(row, column);

            if (sourceSlotData == null)
            {
                return null;
            }
            
            if (IsSlotUsed(sourceSlotData))
            {
                return null;
            }
            
            M3SlotData leftSlotData;
            M3SlotData rightSlotData;
            
            M3Combination result = new M3Combination(M3CombinationOrientation.Horizontal);
            
            result.Add(sourceSlotData);
            _usedSlots.Add(sourceSlotData);
            
            M3TokenTypeData targetType = _typesDataProvider.GetTokenType(sourceSlotData.TokenData.Type);

            bool moveLeft = true;
            bool moveRight = true;

            int leftColumn = column - 1;
            int rightColumn = column + 1;

            while (moveLeft || moveRight)
            {
                if (moveLeft)
                {
                    leftSlotData = _board.GetSlotAt(row, leftColumn);

                    if (CanSlotBeCombined(leftSlotData, targetType))
                    {
                        result.Add(leftSlotData);
                        _usedSlots.Add(leftSlotData);
                        leftColumn--;
                    }
                    else
                    {
                        moveLeft = false;
                    }
                }
                
                if (moveRight)
                {
                    rightSlotData = _board.GetSlotAt(row, rightColumn);
                    
                    if (CanSlotBeCombined(rightSlotData, targetType))
                    {
                        result.Add(rightSlotData);
                        _usedSlots.Add(rightSlotData);
                        rightColumn++;
                    }
                    else
                    {
                        moveRight = false;
                    }
                }
            }

            if (result.Count < 3)
            {
                return null;
            }

            return result;
        }

        private bool IsSlotUsed(M3SlotData slotData)
        {
            return _usedSlots.Contains(slotData);
        }
        
        private M3Combination GetVerticalCombinationForSlotAt(int row, int column)
        {
            if (!_board.HasSlotWithTokenAt(row, column))
            {
                return null;
            }
            
            if (_board.HasSlotObstacleAt(row, column))
            {
                return null;
            }

            M3SlotData sourceSlotData = _board.GetSlotAt(row, column);

            if (sourceSlotData == null)
            {
                return null;
            }
            
            if (IsSlotUsed(sourceSlotData))
            {
                return null;
            }
            
            M3SlotData upSlotData;
            M3SlotData downSlotData;
            
            M3Combination result = new M3Combination(M3CombinationOrientation.Vertical);
            result.Add(sourceSlotData);
            _usedSlots.Add(sourceSlotData);
            
            M3TokenTypeData targetType = _typesDataProvider.GetTokenType(sourceSlotData.TokenData.Type);

            bool moveUp = true;
            bool moveDown = true;

            int upRow = row - 1;
            int downRow = row + 1;

            while (moveUp || moveDown)
            {
                if (moveUp)
                {
                    upSlotData = _board.GetSlotAt(upRow, column);

                    if (CanSlotBeCombined(upSlotData, targetType))
                    {
                        result.Add(upSlotData);
                        _usedSlots.Add(upSlotData);
                        upRow--;
                    }
                    else
                    {
                        moveUp = false;
                    }
                }
                
                if (moveDown)
                {
                    downSlotData = _board.GetSlotAt(downRow, column);
                    
                    if (CanSlotBeCombined(downSlotData, targetType))
                    {
                        result.Add(downSlotData);
                        _usedSlots.Add(downSlotData);
                        downRow++;
                    }
                    else
                    {
                        moveDown = false;
                    }
                }
            }

            if (result.Count < 3)
            {
                return null;
            }

            return result;
        }
        
        private bool CanSlotBeCombined(M3SlotData slotData, M3TokenTypeData targetType)
        {
            if (slotData == null)
            {
                return false;
            }
            
            if (slotData.HasObstacle)
            {
                return false;
            }
            
            if (!slotData.HasToken)
            {
                return false;
            }
            
            M3TokenTypeData currentType = _typesDataProvider.GetTokenType(slotData.TokenData.Type);
            return currentType.IsCombineWith(targetType.Type);
        }
    }
}