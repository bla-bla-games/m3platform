﻿using System.Collections.Generic;

namespace M3Platform.Core.Combination.Finder
{
    public interface IM3PossibleCombinationsFinder
    {
        List<M3Combination> GetPosibleCombinations();
    }
}