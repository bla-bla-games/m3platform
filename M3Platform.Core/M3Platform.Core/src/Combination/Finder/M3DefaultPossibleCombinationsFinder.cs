﻿using System.Collections.Generic;
using M3Platform.Core.Board;

namespace M3Platform.Core.Combination.Finder
{
    public class M3DefaultPossibleCombinationsFinder : IM3PossibleCombinationsFinder
    {
        private M3Board _board;
        private IM3CombinationsFinder _actualCombinationsFinder;
        
        public M3DefaultPossibleCombinationsFinder(M3Board board, IM3CombinationsFinder actualCombinationsFinder)
        {
            _board = board;
            _actualCombinationsFinder = actualCombinationsFinder;
        }
        
        public List<M3Combination> GetPosibleCombinations()
        {
            List<M3Combination> result = new List<M3Combination>();
            
            for (int row = 0; row < _board.Size.Rows; row++)
            {
                for (int column = 0; column < _board.Size.Columns; column++)
                {
                    M3SlotData slotData = _board.GetSlotAt(row, column);
                    CheckSlot(slotData, ref result);
                }
            }

            return result;
        }

        private void CheckSlot(M3SlotData slotData, ref List<M3Combination> result)
        {
            if (slotData == null)
            {
                return;
            }
            
            if (slotData.HasObstacle)
            {
                return;
            }
            
            if (slotData.HasToken && slotData.TokenData.HasObstacle)
            {
                return;
            }
            
            M3SlotData rightSlotData = _board.GetSlotAt(slotData.Position.Row, slotData.Position.Column + 1);
            M3SlotData bottomSlotData = _board.GetSlotAt(slotData.Position.Row + 1, slotData.Position.Column);

            if (rightSlotData != null && !rightSlotData.HasObstacle)
            {
                List<M3Combination> combinations = TestForCombinations(slotData, rightSlotData);
                result.AddRange(combinations);
            }
            
            if (bottomSlotData != null && !bottomSlotData.HasObstacle)
            {
                List<M3Combination> combinations = TestForCombinations(slotData, bottomSlotData);

                if (combinations != null && combinations.Count > 0)
                {
                    result.AddRange(combinations);
                }
            }
        }

        private List<M3Combination> TestForCombinations(M3SlotData slot1, M3SlotData slot2)
        {
            List<M3Combination> result = new List<M3Combination>();
            
            _board.Swap(slot1.Id, slot2.Id);
            M3Combination combination1 = _actualCombinationsFinder.GetActualCombinationAtPosition(slot1.Position);
            M3Combination combination2 = _actualCombinationsFinder.GetActualCombinationAtPosition(slot2.Position);
            _board.Swap(slot2.Id, slot1.Id);

            if (combination1 != null)
            {
                combination1.Remove(slot1);
                combination1.Add(slot2);
                result.Add(combination1);
            }
            
            if (combination2 != null)
            {
                combination2.Remove(slot2);
                combination2.Add(slot1);
                result.Add(combination2);
            }

            return result;
        }
    }
}