﻿using System.Collections.Generic;
using M3Platform.Core.Board;

namespace M3Platform.Core.Combination.Finder
{
    public interface IM3CombinationsFinder
    {
        List<M3Combination> GetActualCombinations();
        M3Combination GetActualCombinationAtPosition(int row, int column);
        M3Combination GetActualCombinationAtPosition(M3Position position);
    }
}