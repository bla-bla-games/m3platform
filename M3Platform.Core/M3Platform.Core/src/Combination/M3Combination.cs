﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using M3Platform.Core.Board;

namespace M3Platform.Core.Combination
{
    public class M3Combination : ICollection<M3SlotData>
    {
        public int Width { get; private set; }
        public int Height { get; private set; }
        public string TokenType { get; private set; }
        public M3CombinationOrientation Orientation { get; private set; }
        public M3SlotData LeadSlot { get; set; }
        
        private HashSet<M3SlotData> _slots;

        public M3Combination(M3CombinationOrientation orientation)
        {
            _slots = new HashSet<M3SlotData>();
            Orientation = orientation;
        }
        
        public M3Combination()
        {
            _slots = new HashSet<M3SlotData>();
            Orientation = M3CombinationOrientation.Unknown;
        }

        public IEnumerator<M3SlotData> GetEnumerator()
        {
            return _slots.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(M3SlotData slotData)
        {
            _slots.Add(slotData);
            TokenType = slotData.TokenData.Type;
            CalculateSize();
        }

        public void Clear()
        {
            _slots.Clear();
            
            Width = 0;
            Height = 0;
        }

        private void CalculateSize()
        {
            int minRow = _slots.Min(x => x.Position.Row);
            int maxRow = _slots.Max(x => x.Position.Row);
            int minColumn = _slots.Min(x => x.Position.Column);
            int maxColumn = _slots.Max(x => x.Position.Column);
            
            Width = maxColumn - minColumn;
            Height = maxRow - minRow;

            if (_slots.Count > 0)
            {
                Width += 1;
                Height += 1;
            }
        }

        public bool Contains(M3SlotData slotData)
        {
            return _slots.Contains(slotData);
        }

        public void CopyTo(M3SlotData[] array, int arrayIndex)
        {
            _slots.CopyTo(array, arrayIndex);
        }

        public bool Remove(M3SlotData slotData)
        {
            bool result = _slots.Remove(slotData);
            CalculateSize();
            
            return result;
        }

        public void Append(M3Combination сombination)
        {
            foreach (M3SlotData slotData in сombination)
            {
                if (Contains(slotData))
                {
                    continue;
                }

                Add(slotData);
            }
            
            CalculateSize();
        }

        public bool IsAdjacentWith(M3Combination сombination)
        {
            foreach (M3SlotData slotData in сombination)
            {
                if (Contains(slotData))
                {
                    return true;
                }
            }

            return false;
        }
        
        public M3SlotData GetAdjacentSlot(M3Combination сombination)
        {
            foreach (M3SlotData slotData in сombination)
            {
                if (Contains(slotData))
                {
                    return slotData;
                }
            }

            return null;
        }

        public M3Combination Union(M3Combination сombination)
        {
            M3Combination result = new M3Combination();
            result.Append(this);
            result.Append(сombination);

            return result;
        }

        public override string ToString()
        {
            string result = "M3Combination(";

            foreach (M3SlotData slot in _slots)
            {
                result += string.Format("[{0},{1}]", slot.Position.Row, slot.Position.Column);
            }

            return result + ")";
        }

        public int Count
        {
            get { return _slots.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }
    }
}