﻿namespace M3Platform.Core.Utils
{
    public class M3Utilities
    {
        public static string GetSlotIdAt(int row, int column)
        {
            return row + "_" + column;
        }
    }
}