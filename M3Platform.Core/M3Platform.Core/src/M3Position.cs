﻿using System;
using UnityEngine;

namespace M3Platform.Core
{
    [Serializable]
    public struct M3Position
    {
        [SerializeField]
        private int _row;

        [SerializeField]
        private int _column;

        public M3Position(int row, int column)
        {
            _row = row;
            _column = column;
        }

        public void Set(int row, int column)
        {
            _row = row;
            _column = column;
        }

        public override string ToString()
        {
            return string.Format("M3Position({0}, {1})", _row, _column);
        }

        public int Row
        {
            get { return _row; }
        }

        public int Column
        {
            get { return _column; }
        }
    }
}