﻿using System;
using M3Platform.Core.Board;
using UnityEngine;

namespace M3Platform.Core
{
    [Serializable]
    public class M3LevelData
    {
        [SerializeField]
        private M3BoardSize _size;

        [SerializeField]
        private M3SlotData[] _slots;

        public M3BoardSize Size
        {
            get { return _size; }
        }

        public M3SlotData[] Slots
        {
            get { return _slots; }
        }
    }
}