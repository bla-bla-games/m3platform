﻿using M3Platform.Core.Board;
using M3Platform.Core.Board.Token;

namespace M3Platform.Core.Generator
{
    public class M3DefaultTokensGenerator : IM3TokensGenerator
    {
        protected M3Board _board;
        protected IM3ItemTypesDataProvider _typesDataProvider;

        public M3DefaultTokensGenerator(M3Board board, IM3ItemTypesDataProvider typesDataProvider)
        {
            _board = board;
            _typesDataProvider = typesDataProvider;
        }

        public M3TokenData GenerateTokenFor(int row, int column)
        {
            M3TokenTypeData tokenTypeData;
            bool isAcceptableType;

            do
            {
                tokenTypeData = _typesDataProvider.GetRandomTokenType();
                
                isAcceptableType = IsHorizontalAcceptable(row, column, tokenTypeData) && IsVerticalAcceptable(row, column, tokenTypeData);
            }
            while (!isAcceptableType);

            return CreateToken(tokenTypeData.Id);
        }

        public M3TokenData CreateToken(string typeId)
        {
            // TODO: Get item data from pool
            return new M3TokenData(typeId);
        }

        private bool IsHorizontalAcceptable(int row, int column, M3TokenTypeData tokenTypeData)
        {
            M3TokenData tokenData1;
            M3TokenData tokenData2;
            
            M3TokenTypeData tokenTypeData1;
            M3TokenTypeData tokenTypeData2;
            
            // check left
            tokenData1 = _board.GetTokenAt(row, column - 1);
            tokenData2 = _board.GetTokenAt(row, column - 2);

            if (tokenData1 != null && tokenData2 != null)
            {
                tokenTypeData1 = _typesDataProvider.GetTokenType(tokenData1.Type);
                tokenTypeData2 = _typesDataProvider.GetTokenType(tokenData2.Type);

                if (tokenTypeData1.IsCombineWith(tokenTypeData.Type) && tokenTypeData2.IsCombineWith(tokenTypeData.Type))
                {
                    return false;
                }
            }
            //
            
            // check right
            tokenData1 = _board.GetTokenAt(row, column + 1);
            tokenData2 = _board.GetTokenAt(row, column + 2);

            if (tokenData1 != null && tokenData2 != null)
            {
                tokenTypeData1 = _typesDataProvider.GetTokenType(tokenData1.Type);
                tokenTypeData2 = _typesDataProvider.GetTokenType(tokenData2.Type);
            
                if (tokenTypeData1.IsCombineWith(tokenTypeData.Type) && tokenTypeData2.IsCombineWith(tokenTypeData.Type))
                {
                    return false;
                }
            }
            //

            return true;
        }
        
        private bool IsVerticalAcceptable(int row, int column, M3TokenTypeData tokenTypeData)
        {
            M3TokenData tokenData1;
            M3TokenData tokenData2;
            
            M3TokenTypeData tokenTypeData1;
            M3TokenTypeData tokenTypeData2;
            
            // check top
            tokenData1 = _board.GetTokenAt(row - 1, column);
            tokenData2 = _board.GetTokenAt(row - 2, column);

            if (tokenData1 != null && tokenData2 != null)
            {
                tokenTypeData1 = _typesDataProvider.GetTokenType(tokenData1.Type);
                tokenTypeData2 = _typesDataProvider.GetTokenType(tokenData2.Type);

                if (tokenTypeData1.IsCombineWith(tokenTypeData.Type) && tokenTypeData2.IsCombineWith(tokenTypeData.Type))
                {
                    return false;
                }
            }
            //
            
            // check bottom
            tokenData1 = _board.GetTokenAt(row + 1, column);
            tokenData2 = _board.GetTokenAt(row + 2, column);

            if (tokenData1 != null && tokenData2 != null)
            {
                tokenTypeData1 = _typesDataProvider.GetTokenType(tokenData1.Type);
                tokenTypeData2 = _typesDataProvider.GetTokenType(tokenData2.Type);
            
                if (tokenTypeData1.IsCombineWith(tokenTypeData.Type) && tokenTypeData2.IsCombineWith(tokenTypeData.Type))
                {
                    return false;
                }
            }
            //

            return true;
        }

        public M3TokenData GenerateTokenFor(M3Position position)
        {
            return GenerateTokenFor(position.Row, position.Column);
        }
    }
}