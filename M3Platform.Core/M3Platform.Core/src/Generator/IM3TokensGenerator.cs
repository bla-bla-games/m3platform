﻿using M3Platform.Core.Board.Token;

namespace M3Platform.Core.Generator
{
    public interface IM3TokensGenerator
    {
        M3TokenData GenerateTokenFor(int row, int column);
        M3TokenData GenerateTokenFor(M3Position position);
        M3TokenData CreateToken(string typeId);
    }
}