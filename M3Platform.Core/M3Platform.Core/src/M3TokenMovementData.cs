﻿﻿using M3Platform.Core.Board.Token;

namespace M3Platform.Core
{
    public class M3TokenMovementData
    {
        public M3TokenData TokenData { get; private set; }
        public M3Path Path { get; private set; }

        public M3TokenMovementData(M3TokenData tokenData)
        {
            TokenData = tokenData;
            Path = new M3Path();
        }

        public void SetPath(M3Path path)
        {
            Path = path;
        }
    }
}