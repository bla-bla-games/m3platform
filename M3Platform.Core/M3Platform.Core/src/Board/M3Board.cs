﻿﻿using System;
using System.Collections.Generic;
using System.Text;
using M3Platform.Core.Board.Token;
 using M3Platform.Core.Combination;
 using UnityEngine;

namespace M3Platform.Core.Board
{
    public class M3SwapInfo
    {
        public M3SlotData Slot1 { get; private set; }
        public M3SlotData Slot2 { get; private set; }

        public void Set(M3SlotData slot1, M3SlotData slot2)
        {
            Slot1 = slot1;
            Slot2 = slot2;
        }
    }
    
    public class M3Board
    {
        public event Action OnChanged;

        public M3BoardSize Size { get; private set; }
        public M3SwapInfo LastSwapInfo { get; private set; }

        private M3SlotData[,] _grid;
        private Dictionary<string, M3SlotData> _slotsMap;

        public M3Board()
        {
            Size = new M3BoardSize();
            LastSwapInfo = new M3SwapInfo();
            _slotsMap = new Dictionary<string, M3SlotData>();
        }

        private void SendChangeNotification()
        {
            if (OnChanged != null)
            {
                OnChanged();
            }
        }

        public void Configure(int rows, int columns)
        {
            Size.Set(rows, columns);
            _grid = new M3SlotData[rows, columns];
            _slotsMap.Clear();

            for (int row = 0; row < Size.Rows; row++)
            {
                for (int column = 0; column < Size.Columns; column++)
                {
                    // TODO: Get SlotData from pool
                    M3SlotData slotData = new M3SlotData(row, column);
                    _grid[row, column] = slotData;
                    _slotsMap.Add(slotData.Id, slotData);
                }
            }

            SendChangeNotification();
        }

        /// <summary>
        /// Overrides token at specified slot.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="tokenData"></param>
        public void InsertTokenAt(int row, int column, M3TokenData tokenData)
        {
            M3SlotData slotData = _grid[row, column];
            slotData.SetToken(tokenData);
            SendChangeNotification();
        }
        
        public void InsertTokenAt(M3Position position, M3TokenData tokenData)
        {
            InsertTokenAt(position.Row, position.Column, tokenData);
        }

        /// <summary>
        /// Swaps tokens between two specified slots.
        /// </summary>
        /// <param name="slotId1"></param>
        /// <param name="slotId2"></param>
        public bool Swap(string slotId1, string slotId2)
        {
            M3SlotData slotData1 = _slotsMap[slotId1];
            M3SlotData slotData2 = _slotsMap[slotId2];

            if (slotData1 == null || slotData2 == null)
            {
                return false;
            }

            if (!slotData1.HasToken && !slotData2.HasToken)
            {
                return false;
            }

            M3TokenData cachedM3TokenData = slotData1.TokenData;

            slotData1.SetToken(slotData2.TokenData);
            slotData2.SetToken(cachedM3TokenData);

            LastSwapInfo.Set(slotData1, slotData2);

            SendChangeNotification();
            return true;
        }

        /// <summary>
        /// Clears a specified slot.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        public void ClearSlotAt(int row, int column)
        {
            if (!HasSlotAt(row, column))
            {
                return;
            }

            if (!HasTokenAt(row, column))
            {
                return;
            }

            GetSlotAt(row, column).Clear();
            SendChangeNotification();
        }

        public void ClearSlotAt(M3Position position)
        {
            ClearSlotAt(position.Row, position.Column);
        }

        /// <summary>
        /// Clears all slots of the board.
        /// </summary>
        public void Clear()
        {
            for (int row = 0; row < Size.Rows; row++)
            {
                for (int column = 0; column < Size.Columns; column++)
                {
                    ClearSlotAt(row, column);
                }
            }

            SendChangeNotification();
        }

        /// <summary>
        /// Returns a slot at specified position.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public M3SlotData GetSlotAt(int row, int column)
        {
            if (row < 0 || row >= Size.Rows)
            {
                return null;
            }
            
            if (column < 0 || column >= Size.Columns)
            {
                return null;
            }
            
            return _grid[row, column];
        }
        
        public M3SlotData GetSlotAt(M3Position position)
        {
            return GetSlotAt(position.Row, position.Column);
        }
        
        public M3SlotData GetSlot(string id)
        {
            return _slotsMap[id];
        }

        public List<M3SlotData> GetSlotsAroundCombination(M3Combination combination)
        {
            List<M3SlotData> result = new List<M3SlotData>();
            
            foreach (M3SlotData slotData in combination)
            {
                M3SlotData topSlotData = GetSlotAt(slotData.Position.Row - 1, slotData.Position.Column);
                M3SlotData bottomSlotData = GetSlotAt(slotData.Position.Row + 1, slotData.Position.Column);

                M3SlotData leftSlotData = GetSlotAt(slotData.Position.Row, slotData.Position.Column - 1);
                M3SlotData rightSlotData = GetSlotAt(slotData.Position.Row, slotData.Position.Column + 1);

                if (topSlotData != null)
                {
                    result.Add(topSlotData);
                }
                
                if (bottomSlotData != null)
                {
                    result.Add(bottomSlotData);
                }
                
                if (!combination.Contains(leftSlotData) && leftSlotData != null)
                {
                    result.Add(leftSlotData);
                }
                
                if (!combination.Contains(rightSlotData) && rightSlotData != null)
                {
                    result.Add(rightSlotData);
                }
            }

            return result;
        }

        /// <summary>
        /// Returns token data of a slot at specified position.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public M3TokenData GetTokenAt(int row, int column)
        {
            if (!HasSlotAt(row, column))
            {
                return null;
            }

            return GetSlotAt(row, column).TokenData;
        }

        /// <summary>
        /// Indicates whether slot is exist at specified position.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public bool HasSlotAt(int row, int column)
        {
            if (row < 0 || row > Size.Rows - 1)
            {
                return false;
            }
            
            if (column < 0 || column > Size.Columns - 1)
            {
                return false;
            }
            
            return _grid[row, column] != null;
        }
        
        public bool HasSlotAt(M3Position position)
        {
            return HasSlotAt(position.Row, position.Column);
        }

        /// <summary>
        /// Indicates whether slot have a token at specified position.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public bool HasTokenAt(int row, int column)
        {
            if (!HasSlotAt(row, column))
            {
                return false;
            }

            M3SlotData slotData = GetSlotAt(row, column);
            return slotData.HasToken;
        }

        /// <summary>
        /// Indicates whether slot is exist with token at specified position.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public bool HasSlotWithTokenAt(int row, int column)
        {
            if (!HasSlotAt(row, column))
            {
                return false;
            }

            if (!HasTokenAt(row, column))
            {
                return false;
            }

            return true;
        }

        public bool HasSlotObstacleAt(int row, int column)
        {
            if (!HasSlotAt(row, column))
            {
                return false;
            }

            return GetSlotAt(row, column).HasObstacle;
        }
        
        public bool HasTokenObstacleAt(int row, int column)
        {
            if (HasSlotObstacleAt(row, column))
            {
                return true;
            }

            if (!HasTokenAt(row, column))
            {
                return false;
            }

            return GetSlotAt(row, column).TokenData.HasObstacle;
        }
        
        public bool HasObstacleAt(int row, int column)
        {
            return HasSlotObstacleAt(row, column) || HasTokenObstacleAt(row, column);
        }
        
        public bool HasObstacleAt(M3Position position)
        {
            return HasObstacleAt(position.Row, position.Column);
        }

        public void Snapshot()
        {
            StringBuilder result = new StringBuilder();

            for (int row = 0; row < Size.Rows; row++)
            {
                for (int column = 0; column < Size.Columns; column++)
                {
                    string itemLog = " ";
                    M3SlotData m3SlotData = GetSlotAt(row, column);

                    if (m3SlotData != null)
                    {
                        itemLog = "[_]";

                        if (m3SlotData.TokenData != null)
                        {
                            itemLog = string.Format("[{0}]", m3SlotData.TokenData.Type);
                        }
                    }

                    result.Append(itemLog);
                }

                result.Append("\n");
            }

            Debug.Log(result);
        }
    }
}