﻿namespace M3Platform.Core.Board
{
    public interface IM3Triggerable
    {
        void Trigger();
    }
}