﻿namespace M3Platform.Core.Board.Obstacle
{
    public class M3ObstacleTypeData
    {
        public string Id { get; private set; }
        public int Health { get; private set; }

        public M3ObstacleTypeData(string id, int health)
        {
            Id = id;
            Health = health;
        }

        public bool IsDestroyable
        {
            get { return Health > 0; }
        }
    }
}