﻿namespace M3Platform.Core.Board.Obstacle
{
    public class M3ObstacleData
    {
        private string _id;
        private string _type;
        private int _health;

        public M3ObstacleData(string type, int health = -1)
        {
            _id = GetHashCode().ToString();
            _type = type;
            _health = health;
        }

        public void DecreaseHealth()
        {
            _health--;
        }

        public string Id
        {
            get { return _id; }
        }

        public string Type
        {
            get { return _type; }
        }
        
        public int Health
        {
            get { return _health; }
        }
    }
}