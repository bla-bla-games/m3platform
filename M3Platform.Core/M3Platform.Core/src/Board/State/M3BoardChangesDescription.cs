﻿using System.Collections.Generic;

namespace M3Platform.Core.Board.State
{
    public class M3BoardChangesDescription
    {
        public List<M3CraftBonusDescription> CraftedBonuses { get; private set; }
        public List<M3GatherTokenDescription> GatherTokensFromSlots { get; private set; }
        public List<M3TriggerSlotObstacleDescription> TriggeredSlotObstacles { get; private set; }
        public List<M3TriggerTokenObstacleDescription> TriggeredTokenObstacles { get; private set; }

        public M3BoardChangesDescription()
        {
            CraftedBonuses = new List<M3CraftBonusDescription>();
            GatherTokensFromSlots = new List<M3GatherTokenDescription>();
            TriggeredSlotObstacles = new List<M3TriggerSlotObstacleDescription>();
            TriggeredTokenObstacles = new List<M3TriggerTokenObstacleDescription>();
        }
    }
}