﻿using M3Platform.Core.Board.Obstacle;

namespace M3Platform.Core.Board.State
{
    public class M3TriggerSlotObstacleDescription
    {
        public string SlotId { get; private set; }
        public M3ObstacleData ObstacleData { get; private set; }
        public int PreviousHealth { get; private set; }

        public M3TriggerSlotObstacleDescription(string slotId, M3ObstacleData obstacleData, int previousHealth)
        {
            SlotId = slotId;
            ObstacleData = obstacleData;
            PreviousHealth = previousHealth;
        }
    }
}