﻿using M3Platform.Core.Combination;

namespace M3Platform.Core.Board.State
{
    public class M3CraftBonusDescription
    {
        public string BonusTokenTypeId { get; private set; }
        public M3Combination Combination { get; private set; }

        public M3CraftBonusDescription(string bonusTokenTypeId, M3Combination combination)
        {
            BonusTokenTypeId = bonusTokenTypeId;
            Combination = combination;
        }
    }
}