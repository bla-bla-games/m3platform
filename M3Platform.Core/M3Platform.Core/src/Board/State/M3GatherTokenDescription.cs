﻿using M3Platform.Core.Board.Token;

namespace M3Platform.Core.Board.State
{
    public class M3GatherTokenDescription
    {
        public string SlotId { get; private set; }
        public M3TokenData TokenData { get; private set; }

        public M3GatherTokenDescription(string slotId, M3TokenData tokenData)
        {
            SlotId = slotId;
            TokenData = tokenData;
        }
    }
}