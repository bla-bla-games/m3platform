﻿using M3Platform.Core.Board.Obstacle;
using M3Platform.Core.Board.Token;

namespace M3Platform.Core.Board.State
{
    public class M3TriggerTokenObstacleDescription
    {
        public string SlotId { get; private set; }
        public string TokenData { get; private set; }
        public M3ObstacleData ObstacleData { get; private set; }
        public int PreviousHealth { get; private set; }

        public M3TriggerTokenObstacleDescription(string slotId, M3TokenData tokenData, M3ObstacleData obstacleData, int previousHealth)
        {
            SlotId = slotId;
            TokenData = slotId;
            ObstacleData = obstacleData;
            PreviousHealth = previousHealth;
        }
    }
}