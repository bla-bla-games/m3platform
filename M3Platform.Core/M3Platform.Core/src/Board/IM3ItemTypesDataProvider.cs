﻿using M3Platform.Core.Board.Obstacle;
using M3Platform.Core.Board.Token;

namespace M3Platform.Core.Board
{
    public interface IM3ItemTypesDataProvider
    {
        M3TokenTypeData GetTokenType(string id);
        M3ObstacleTypeData GetObstacleType(string id);
        M3TokenTypeData GetRandomTokenType();
    }
}