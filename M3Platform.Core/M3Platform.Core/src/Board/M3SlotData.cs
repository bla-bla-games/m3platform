﻿using M3Platform.Core.Board.Token;
using M3Platform.Core.Utils;

namespace M3Platform.Core.Board
{
    public class M3SlotData : M3BoardItemData
    {
        private string _id;
        private M3Position _position;
        private M3TokenData _tokenData;

        public M3SlotData(int row, int column)
        {
            _id = M3Utilities.GetSlotIdAt(row, column);
            _position = new M3Position(row, column);
        }

        public void SetToken(M3TokenData tokenData)
        {
            _tokenData = tokenData;
        }
        
        public void Clear()
        {
            _tokenData = null;
        }

        public bool HasToken
        {
            get { return TokenData != null; }
        }
        
        public bool IsEmpty
        {
            get { return !HasToken && !HasObstacle; }
        }

        public string Id
        {
            get { return _id; }
        }

        public M3Position Position
        {
            get { return _position; }
        }

        public M3TokenData TokenData
        {
            get { return _tokenData; }
        }
    }
}