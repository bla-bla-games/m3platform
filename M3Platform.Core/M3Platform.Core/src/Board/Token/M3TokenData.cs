﻿using System;
using UnityEngine;

namespace M3Platform.Core.Board.Token
{
    [Serializable]
    public class M3TokenData : M3BoardItemData
    {
        [SerializeField]
        private string _id;

        [SerializeField]
        private string _type;

        public M3TokenData(string type)
        {
            _id = GetHashCode().ToString();
            _type = type;
        }

        public string Id
        {
            get { return _id; }
        }

        public string Type
        {
            get { return _type; }
        }
    }
}