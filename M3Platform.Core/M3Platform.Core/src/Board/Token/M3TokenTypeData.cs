﻿namespace M3Platform.Core.Board.Token
{
    public class M3TokenTypeData
    {
        // max available bitmask 1 << 31
        private static int _nextMask = 1;

        public string Id { get; private set; }
        public int Type { get; private set; }
        public bool IsBonus { get; private set; }
        public string BonusType { get; private set; }
        public int CombinationMask { get; set; }

        public M3TokenTypeData(string id, int customTypeMask = -1, string bonusType = null)
        {
            Id = id;
            BonusType = bonusType;
            IsBonus = !string.IsNullOrEmpty(bonusType);

            if (customTypeMask < 0)
            {
                Type = _nextMask;
                _nextMask <<= 1;
            }
            else
            {
                Type = customTypeMask;
            }

            // by default combine only with same type
            CombinationMask = Type;
        }

        public bool IsCombineWith(int type)
        {
            return (CombinationMask & type) != 0;
        }
    }
}