﻿using System.Collections.Generic;
using System.Linq;
using M3Platform.Core.Board.Obstacle;

namespace M3Platform.Core.Board
{
    public class M3BoardItemData
    {
        private Stack<M3ObstacleData> _obstacles;

        public M3BoardItemData()
        {
            _obstacles = new Stack<M3ObstacleData>();
        }
        
        public void AddObstacle(M3ObstacleData obstacleData)
        {
            _obstacles.Push(obstacleData);
        }
        
        public M3ObstacleData GetObstacle()
        {
            if (!HasObstacle)
            {
                return null;
            }

            return _obstacles.Peek();
        }
        
        public List<M3ObstacleData> GetObstacles()
        {
            return _obstacles.ToList();
        }
        
        public void RemoveObstacle()
        {
            if (_obstacles.Count <= 0)
            {
                return;
            }

            _obstacles.Pop();
        }
        
        public bool HasObstacle
        {
            get { return _obstacles.Count > 0; }
        }
    }
}