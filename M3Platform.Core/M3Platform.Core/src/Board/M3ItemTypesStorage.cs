﻿using System.Collections.Generic;
using System.Linq;
using M3Platform.Core.Board.Obstacle;
using M3Platform.Core.Board.Token;
using UnityEngine;

namespace M3Platform.Core.Board
{
    public class M3ItemTypesStorage : IM3ItemTypesDataProvider
    {
        private List<M3TokenTypeData> _tokenTypesList;
        private Dictionary<string, M3TokenTypeData> _tokenTypesMap;
        
        private List<M3ObstacleTypeData> _obstacleTypesList;
        private Dictionary<string, M3ObstacleTypeData> _obstacleTypesMap;
        
        private Dictionary<string, Dictionary<string, string>> _bonusCraftsMap;

        public M3ItemTypesStorage()
        {
            _tokenTypesList = new List<M3TokenTypeData>();
            _tokenTypesMap = new Dictionary<string, M3TokenTypeData>();
            
            _obstacleTypesList = new List<M3ObstacleTypeData>();
            _obstacleTypesMap = new Dictionary<string, M3ObstacleTypeData>();
            
            _bonusCraftsMap = new Dictionary<string, Dictionary<string, string>>();
        }

        public void RegisterTokenType(M3TokenTypeData typeData)
        {
            if (IsTokenTypeRegistered(typeData.Id))
            {
                Debug.LogWarning(string.Format("Token type '{0}' is already registered!", typeData.Id));
                return;
            }

            _tokenTypesList.Add(typeData);
            _tokenTypesMap.Add(typeData.Id, typeData);
        }
        
        public void RegisterObstacleType(M3ObstacleTypeData typeData)
        {
            if (IsObstacleTypeRegistered(typeData.Id))
            {
                Debug.LogWarning(string.Format("Obstacle type '{0}' is already registered!", typeData.Id));
                return;
            }

            _obstacleTypesList.Add(typeData);
            _obstacleTypesMap.Add(typeData.Id, typeData);
        }

        public void RegisterBonusCraftInfo(string tokenTypeId, string bonusTypeId, string bonusTokenTypeId)
        {
            if (!_bonusCraftsMap.ContainsKey(tokenTypeId))
            {
                _bonusCraftsMap.Add(tokenTypeId, new Dictionary<string, string>());
            }
            
            _bonusCraftsMap[tokenTypeId].Add(bonusTypeId, bonusTokenTypeId);
        }

        public M3TokenTypeData UnregisterTokenType(string id)
        {
            if (!IsTokenTypeRegistered(id))
            {
                Debug.LogWarning(string.Format("Token type '{0}' was not registered!", id));
                return null;
            }

            M3TokenTypeData typeData = _tokenTypesMap[id];

            _tokenTypesMap.Remove(id);
            _tokenTypesList.Remove(typeData);

            return typeData;
        }
        
        public M3ObstacleTypeData UnregisterObstacleType(string id)
        {
            if (!IsObstacleTypeRegistered(id))
            {
                Debug.LogWarning(string.Format("Obstacle type '{0}' was not registered!", id));
                return null;
            }

            M3ObstacleTypeData typeData = _obstacleTypesMap[id];

            _obstacleTypesMap.Remove(id);
            _obstacleTypesList.Remove(typeData);

            return typeData;
        }

        public bool IsTokenTypeRegistered(string id)
        {
            return _tokenTypesMap.ContainsKey(id);
        }
        
        public bool IsObstacleTypeRegistered(string id)
        {
            return _obstacleTypesMap.ContainsKey(id);
        }
        
        public string GetBonusTokenTypeId(string tokenTypeId, string bonusTypeId)
        {
            if (!_bonusCraftsMap.ContainsKey(tokenTypeId))
            {
                return null;
            }
            
            if (!_bonusCraftsMap[tokenTypeId].ContainsKey(bonusTypeId))
            {
                return null;
            }

            return _bonusCraftsMap[tokenTypeId][bonusTypeId];
        }

        public M3TokenTypeData GetTokenType(string id)
        {
            if (!IsTokenTypeRegistered(id))
            {
                return null;
            }

            return _tokenTypesMap[id];
        }
        
        public M3ObstacleTypeData GetObstacleType(string id)
        {
            if (!IsObstacleTypeRegistered(id))
            {
                return null;
            }

            return _obstacleTypesMap[id];
        }

        public M3TokenTypeData GetRandomTokenType()
        {
            List<M3TokenTypeData> types = _tokenTypesList.Where(x => !x.IsBonus).ToList();
            int index = Random.Range(0, types.Count);
            M3TokenTypeData result = types[index];
            return result;
        }

        public int TokenTypesCount
        {
            get { return _tokenTypesList.Count; }
        }
        
        public int ObstacleTypesCount
        {
            get { return _obstacleTypesList.Count; }
        }
    }
}