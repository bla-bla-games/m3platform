﻿using System;
using UnityEngine;

namespace M3Platform.Core.Board
{
    [Serializable]
    public class M3BoardSize
    {
        [SerializeField]
        private int _rows;

        [SerializeField]
        private int _columns;

        public M3BoardSize()
        {
            Set(0, 0);
        }

        public M3BoardSize(int rows, int columns)
        {
            Set(rows, columns);
        }

        public void Set(int rows, int columns)
        {
            _rows = rows;
            _columns = columns;
        }

        public int Rows
        {
            get { return _rows; }
        }

        public int Columns
        {
            get { return _columns; }
        }
    }
}