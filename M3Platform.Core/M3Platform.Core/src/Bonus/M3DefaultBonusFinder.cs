﻿using System.Collections.Generic;
using System.Linq;
using M3Platform.Core.Bonus.Recognizers;
using M3Platform.Core.Combination;

namespace M3Platform.Core.Bonus
{
    public class M3DefaultBonusFinder : IM3BonusFinder
    {
        private List<M3BonusRecognizer> _recognizers;

        public M3DefaultBonusFinder()
        {
            _recognizers = new List<M3BonusRecognizer>();
        }
        
        public void registerRecognizer(M3BonusRecognizer recognizer)
        {
            _recognizers.Add(recognizer);
            _recognizers.OrderByDescending(x => x.Priority);
        }

        public string FindBonusIn(M3Combination combination)
        {
            foreach (M3BonusRecognizer recognizer in _recognizers)
            {
                if (recognizer.Recognize(combination))
                {
                    return recognizer.BonusId;
                }
            }
            
            return null;
        }
    }
}