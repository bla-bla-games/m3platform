﻿using M3Platform.Core.Bonus.Recognizers;
using M3Platform.Core.Combination;

namespace M3Platform.Core.Bonus
{
    public interface IM3BonusFinder
    {
        void registerRecognizer(M3BonusRecognizer recognizer);
        string FindBonusIn(M3Combination combination);
    }
}