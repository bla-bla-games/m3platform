﻿using M3Platform.Core.Combination;

namespace M3Platform.Core.Bonus.Recognizers
{
    public class M3LineH4Recognizer : M3BonusRecognizer
    {
        public M3LineH4Recognizer(string bonusId, int priority) : base(bonusId, priority)
        {
        }

        public override bool Recognize(M3Combination combination)
        {
            if (combination.Width < 4)
            {
                return false;
            }
            
            if (combination.Height != 1)
            {
                return false;
            }
            
            return true;
        }
    }
}