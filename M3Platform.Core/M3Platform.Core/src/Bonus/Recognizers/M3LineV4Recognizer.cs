﻿using M3Platform.Core.Combination;

namespace M3Platform.Core.Bonus.Recognizers
{
    public class M3LineV4Recognizer : M3BonusRecognizer
    {
        public M3LineV4Recognizer(string bonusId, int priority) : base(bonusId, priority)
        {
        }

        public override bool Recognize(M3Combination combination)
        {
            if (combination.Height < 4)
            {
                return false;
            }
            
            if (combination.Width != 1)
            {
                return false;
            }
            
            return true;
        }
    }
}