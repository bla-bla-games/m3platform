﻿using M3Platform.Core.Combination;

namespace M3Platform.Core.Bonus.Recognizers
{
    public abstract class M3BonusRecognizer
    {
        private int _priority;
        private string _bonusId;

        public M3BonusRecognizer(string bonusId, int priority)
        {
            _bonusId = bonusId;
            _priority = priority;
        }
        
        public abstract bool Recognize(M3Combination combination);
        
        public string BonusId
        {
            get { return _bonusId; }
        }

        public int Priority
        {
            get { return _priority; }
        }
    }
}