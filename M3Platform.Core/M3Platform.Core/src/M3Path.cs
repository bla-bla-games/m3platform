﻿﻿using System.Collections;
using System.Collections.Generic;

namespace M3Platform.Core
{
    public class M3Path : ICollection<M3Position>
    {
        private List<M3Position> _waypoints;

        public M3Path()
        {
            _waypoints = new List<M3Position>();
        }

        public IEnumerator<M3Position> GetEnumerator()
        {
            return _waypoints.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(M3Position waypoint)
        {
            _waypoints.Add(waypoint);
        }

        public void Clear()
        {
            _waypoints.Clear();
        }

        public bool Contains(M3Position waypoint)
        {
            return _waypoints.Contains(waypoint);
        }

        public void CopyTo(M3Position[] array, int arrayIndex)
        {
            _waypoints.CopyTo(array, arrayIndex);
        }

        public bool Remove(M3Position waypoint)
        {
            return _waypoints.Remove(waypoint);
        }
        
        public M3Position this[int index]
        {
            get { return _waypoints[index]; }
        }

        public M3Position First
        {
            get { return this[0]; }
        }
        
        public M3Position Last
        {
            get
            {
                return this[Count - 1];
            }
        }
        
        public override bool Equals(object value)
        {
            M3Path path = value as M3Path;

            if (ReferenceEquals(null, path))
            {
                return false;
            }

            for (int i = 0; i < _waypoints.Count; i++)
            {
                if (!_waypoints[i].Equals(path[i]))
                {
                    return false;
                }
            }

            return true;
        }

        public override string ToString()
        {
            string result = "M3Path(";

            foreach (M3Position waypoint in _waypoints)
            {
                result += string.Format("[{0},{1}]", waypoint.Row, waypoint.Column);
            }

            return result + ")";
        }

        public int Count
        {
            get { return _waypoints.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }
    }
}