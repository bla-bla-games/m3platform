﻿﻿using System.Collections.Generic;
using M3Platform.Core.Board;
using M3Platform.Core.Board.Token;
using M3Platform.Core.Generator;

namespace M3Platform.Core.Filler
{
    public class M3DefaultEmptySlotsFiller : IM3EmptySlotsFiller
    {
        public List<M3TokenMovementData> CollapseFloatingTokens(M3Board board)
        {
            List<M3TokenMovementData> result = new List<M3TokenMovementData>();
            M3TokenMovementData movementData;

            for (int column = 0; column < board.Size.Columns; column++)
            {
                for (int row = board.Size.Rows - 1; row >= 0; row--)
                {
                    if (!board.HasSlotAt(row, column))
                    {
                        continue;
                    }
                    
                    M3SlotData sourceSlot = board.GetSlotAt(row, column);
                    
                    if (board.HasSlotObstacleAt(row, column))
                    {
                        continue;
                    }
                    
                    if (!board.HasTokenAt(row, column))
                    {
                        continue;
                    }
                    
                    M3Path path = new M3Path();
                    FindPath(board, sourceSlot, ref path);

                    if (path.Count <= 1)
                    {
                        continue;
                    }
                    
                    movementData = new M3TokenMovementData(sourceSlot.TokenData);
                    movementData.SetPath(path);
                    result.Add(movementData);

                    M3SlotData targetSlot = board.GetSlotAt(path.Last);
                    board.Swap(sourceSlot.Id, targetSlot.Id);
                }
            }

            return result;
        }

        public List<M3TokenMovementData> DropNewTokens(M3Board board, IM3TokensGenerator tokensGenerator)
        {
            List<M3TokenMovementData> result = new List<M3TokenMovementData>();

            M3Position fromPosition = new M3Position();
            M3Position toPosition = new M3Position();
            M3TokenMovementData movementData;

            int counter = 1;
            M3Path path;  

            for (int column = 0; column < board.Size.Columns; column++)
            {
                bool hasEmptySlot = board.HasSlotAt(0, column) 
                    && !board.HasSlotObstacleAt(0, column) 
                    && !board.HasTokenAt(0, column);

                while (hasEmptySlot)
                {
                    M3SlotData source = board.GetSlotAt(0, column);
                    path = new M3Path();
                    path.Add(new M3Position(-1 * counter, column));
                    FindPath(board, source, ref path);

                    if (path.Count <= 1)
                    {
                        break;
                    }

                    M3TokenData tokenData = tokensGenerator.GenerateTokenFor(path.Last.Row, column);

                    movementData = new M3TokenMovementData(tokenData);
                    movementData.SetPath(path);
                    result.Add(movementData);
                    board.InsertTokenAt(path.Last.Row, column, tokenData);
                    
                    counter++;
                    hasEmptySlot = board.HasSlotAt(0, column) 
                                   && !board.HasSlotObstacleAt(0, column) 
                                   && !board.HasTokenAt(0, column);
                }
                
                counter = 1;
            }

            return result;
        }

        private void FindPath(M3Board board, M3SlotData fromSlot, ref M3Path path)
        {
            path.Add(fromSlot.Position);

            int nextRow = fromSlot.Position.Row + 1;

            if (!board.HasSlotAt(nextRow, fromSlot.Position.Column))
            {
                return;
            }
            
            if (board.HasSlotObstacleAt(nextRow, fromSlot.Position.Column))
            {
                return;
            }
            
            if (board.HasTokenAt(nextRow, fromSlot.Position.Column))
            {
                return;
            }

            M3SlotData nextSlot = board.GetSlotAt(nextRow, fromSlot.Position.Column);
            FindPath(board, nextSlot, ref path);
        }
    }
}