﻿using System.Collections.Generic;
using M3Platform.Core.Board;
using M3Platform.Core.Generator;

namespace M3Platform.Core.Filler
{
    public interface IM3EmptySlotsFiller
    {
        /// <summary>
        /// Processes all tokens that are don't have any support to stay at the position.
        /// Returns path by which tokens was moved.
        /// </summary>
        /// <param name="board"></param>
        /// <returns></returns>
        List<M3TokenMovementData> CollapseFloatingTokens(M3Board board);

        /// <summary>
        /// Fills all remaining slots.
        /// Returns path by which tokens was moved.
        /// </summary>
        /// <param name="board"></param>
        /// <param name="tokensGenerator"></param>
        /// <returns></returns>
        List<M3TokenMovementData> DropNewTokens(M3Board board, IM3TokensGenerator tokensGenerator);
    }
}